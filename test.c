#include "ccm-siv.h"
#include <stdio.h>


int main(void); //Main test function
void hexdump(uint8_t* bytes, uint32_t len); //Print 4B-aligned hexdump



int main(void) {

	uint8_t key[] = { // Master key (used for AES-128, AES-192 and AES-256
		0x23, 0x39, 0xBD, 0xF2,//128
		0x06, 0xBB, 0xBA, 0x94,
		0x14, 0x80, 0x7B, 0x92,
		0x6B, 0xBF, 0xE0, 0x71,

		0xFD, 0xED, 0x18, 0x04,//192
		0x11, 0xED, 0xD4, 0x59,

		0x47, 0xF9, 0xC8, 0xE0,//256
		0x19, 0xDA, 0x3C, 0x30
	};

	struct AES_CCM_SIV_ctx ctx; //Context struct to hold derived subkeys K1, K2, K3

	uint8_t A[] = { //Additional Data
		0xAD, 0xAD, 0xAD, 0xAD,
		0xAD, 0xAD, 0xAD, 0xAD,
		0xAD, 0xAD, 0xAD, 0xAD,
		0xAD, 0xAD, 0xAD, 0xAD,

		0xAA, 0xAA, 0xAA, 0xAA,
		0xAA, 0xAA, 0xAA, 0xAA,
		0xAA, 0xAA, 0xAA, 0xAA,
		0xAA, 0xAA, 0xAA, 0xAA,

		0xDA, 0xDA, 0xDA, 0xDA,
		0xDA, 0xDA, 0xDA
	};

	uint32_t a = 39; //Length of Additional Data

	uint8_t P[] = { //Plaintext
		0x00, 0x11, 0x22, 0x33,
		0x44, 0x55, 0x66, 0x77,
		0x88, 0x99, 0xAA, 0xBB,
		0xCC, 0xDD, 0xEE, 0xFF,

		0xF0, 0xF1, 0xF2, 0xF3,
		0xF4, 0xF5, 0xF6, 0xF7,
		0xF8, 0xF9, 0xFA, 0xFB,
		0xFC, 0xFD, 0xFE, 0xFF,

		0xDE, 0xAD, 0xBE, 0xEF,
		0x00
	};

	uint32_t p = 37; //Length of Plaintext

	uint8_t N[16] = {0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01}; //Nonce

	uint8_t T[16] = {0}; //Message Authentication Tag

	bool error; // For output of authenticated decryption function


	/*
	With the following testvalues for a and p, these test cases are covered:
		1) No A and no P
		2) Authentication only, A     multiple of blocksize (= without padding)
		3) Authentication only, A not multiple of blocksize (= with    padding)
		4) Confidentiality only,P     multiple of blocksize (= without padding)
		5) Confidentiality only,P not multiple of blocksize (= with    padding)
		6) A     multiple of blocksize, P not multiple of blocksize
		7) A not multiple of blocksize, P     multiple of blocksize
		8) A     multiple of blocksize, P     multiple of blocksize
		9) A not multiple of blocksize, P not multiple of blocksize
	*/

	uint32_t testvalues_a[] = {0, 16, 19,  0,  0, 16, 19, 16, 39}; // different a values to test
	uint32_t testvalues_p[] = {0,  0,  0, 32, 11, 11, 32, 32, 37}; // different p values to test
	uint32_t number_of_tests = 9; //number of tests


	for(uint32_t i=0; i < number_of_tests; i++) { // run through all testvalues of a and p
		a = testvalues_a[i];
		p = testvalues_p[i];
		printf("#####################################################\n");
		printf("# len(K) = %03d bit                                  #\n", AES_CCM_SIV_KEYSIZE);
		printf("# len(A) =  %02d B                                    #\n", a);
		printf("# len(P) =  %02d B                                    #\n", p);
		printf("#####################################################\n\n");

		printf("AES_CCM_SIV_%d_encrypt:\n", AES_CCM_SIV_KEYSIZE);
		printf("K = {");hexdump(key, AES_CCM_SIV_KEYSIZE/8);printf("}\n");
		AES_CCM_SIV_init(&ctx, key); // Derive subkeys
		printf("k1 = {");hexdump(ctx.k1, AES_CCM_SIV_KEYSIZE/8);printf("}\n");
		printf("k2 = {");hexdump(ctx.k2, AES_CCM_SIV_KEYSIZE/8);printf("}\n");
		printf("k3 = {");hexdump(ctx.k3, AES_CCM_SIV_KEYSIZE/8);printf("}\n");
		printf("A = {");hexdump(A, a);printf("}\n");
		printf("a = %d\n", a);
		printf("P = {");hexdump(P, p);printf("}\n");
		printf("p = %d\n", p);
		printf("N = {");hexdump(N, 16);printf("}\n\n");
		AES_CCM_SIV_encrypt(&ctx, A, a, P, p, N, T); //Encrypt Plaintext buffer P and store tag in T
		printf("C = {");hexdump(P, p);printf("}\n");
		printf("T = {");hexdump(T, 16);printf("}\n\n");

		//Attacker might change C or T here. Feel free to try.

		printf("AES_CCM_SIV_%d_decrypt:\n", AES_CCM_SIV_KEYSIZE);
		error = AES_CCM_SIV_decrypt(&ctx, A, a, P, p, N, T); //Decrypt ciphertext buffer P
		if(error) {
			printf("[ERROR] INVALID MAC!\n");
		} else {
			printf("Signature ok.\n");
			printf("P = {");hexdump(P, p);printf("}\n");
		};
		printf("\n\n");
	}







	return 0;
}








void hexdump(uint8_t* bytes, uint32_t len) {
	while(len > 0) {
		//printf("0x%02X", *bytes); // byte-array style
		//if(len > 1) printf(", "); // byte-array style

		printf("%02X", *bytes); // compact style
		if(len > 1) printf(":"); // compact style

		bytes++;
		len--;
	}
	return;
}
