# CCM-SIV Reference C Implementation

CCM-SIV (Counter Mode with CBC-Based MAC and Synthetic Initialization Vector) is a single-PRF full nonce-misuse-resistant authenticated encryption with additional data scheme (AEAD). The paper can be found here: https://eprint.iacr.org/2019/892.pdf

The reference C implementation implements AES_CCM_SIV_{128,192,256} subkey derivation, authenticated encryption and authenticated decryption algorithms.
It uses a lightweight AES library from github (https://github.com/kokke/tiny-AES-c).

Build:
	make

Clean:
	make clean

Run Test:
	./test

