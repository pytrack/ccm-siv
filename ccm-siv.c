#include "ccm-siv.h"

//#include <stdio.h> //FOR DEBUG
//extern void hexdump(uint8_t* bytes, uint32_t len); //FOR DEBUG

void AES_CCM_SIV_init(struct AES_CCM_SIV_ctx* ctx, uint8_t* key) {

	struct AES_ctx aesctx;
	uint8_t buf[16];

	AES_init_ctx(&aesctx, key);

#if AES_CCM_SIV_KEYSIZE == 128
	memset(buf, 0, 16);
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(ctx->k1, buf, 16);

	memset(buf, 0, 15);
	buf[15] = 1;
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(ctx->k2, buf, 16);

	memset(buf, 0, 15);
	buf[15] = 2;
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(ctx->k3, buf, 16);

#elif AES_CCM_SIV_KEYSIZE == 192
	memset(buf, 0, 16);
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(ctx->k1, buf, 16);

	memset(buf, 0, 15);
	buf[15] = 1;
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(&ctx->k1[16], buf, 8);
	memcpy(ctx->k2, &buf[8], 8);

	memset(buf, 0, 15);
	buf[15] = 2;
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(&ctx->k2[8], buf, 16);

	memset(buf, 0, 15);
	buf[15] = 3;
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(ctx->k3, buf, 16);

	memset(buf, 0, 15);
	buf[15] = 4;
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(&ctx->k3[16], buf, 8);

#else //256bit
	memset(buf, 0, 16);
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(ctx->k1, buf, 16);

	memset(buf, 0, 15);
	buf[15] = 1;
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(&ctx->k1[16], buf, 16);

	memset(buf, 0, 15);
	buf[15] = 2;
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(ctx->k2, buf, 16);

	memset(buf, 0, 15);
	buf[15] = 3;
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(&ctx->k2[16], buf, 16);

	memset(buf, 0, 15);
	buf[15] = 4;
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(ctx->k3, buf, 16);

	memset(buf, 0, 15);
	buf[15] = 5;
	AES_ECB_encrypt(&aesctx, buf);
	memcpy(&ctx->k3[16], buf, 16);

#endif

	return;
};



void AES_CCM_SIV_encrypt(struct AES_CCM_SIV_ctx* ctx, uint8_t* A, uint32_t a, uint8_t* P, uint32_t p, uint8_t* N, uint8_t* T) {

	struct AES_ctx aesctx; //AES context for AES library
	uint8_t* buf; //buffer to hold pad(A), pad(P), L
	uint32_t bufsize; //size of buffer  (unit: bytes)
	uint32_t a_rounded; //a rounded up to multiple of AES block size (unit: bytes)
	uint32_t p_rounded; //p rounded up to multiple of AES block size (unit: bytes)
	uint64_t L[2] = {0}; //Length block L that contains original bitlength of A and P
	uint8_t zeros[16] = {0}; //General purpose zero block


	// Bijective Encoding Function (building up input for CBC-based PRF:
	// -----------------------------------------------------------------

	a_rounded = ((a+15)&~0x0F); //round up to multiple of AES block size
	p_rounded = ((p+15)&~0x0F); //round up to multiple of AES block size
	bufsize = a_rounded + p_rounded + 16; // len(pad(A)) + len(pad(P)) + len(L)
	L[0] = (uint64_t)(a * 8); //original bitlength of A
	L[1] = (uint64_t)(p * 8); //original bitlength of P

	buf = malloc(bufsize); //allocate memory for pad(A), pad(P), L

	memset(buf, 0x00, bufsize); //initialize buffer with zeros
	memcpy(buf, A, a); //copy additional data into buffer
	memcpy(&buf[a_rounded], P, p); //copy plaintext into buffer
	memcpy(&buf[a_rounded + p_rounded], &L[0], 8); //copy bitlength of A into L-block
	memcpy(&buf[a_rounded + p_rounded + 8], &L[1], 8); //copy bitlength of P into L-block

//printf("\n\n[DEBUG] Buffer Dump before Tag calculation:\n");
//hexdump(buf, bufsize);
//printf("\n\n");

	// Calculate CBC-based PRF (MAC):
	// ------------------------------

	AES_init_ctx_iv(&aesctx, ctx->k1, zeros); // initialize AES context with subkey k1 and zero IV
	AES_CBC_encrypt_buffer(&aesctx, buf, bufsize); //encrypt buffer using CBC

	for(int i=0; i<16; i++)
		buf[bufsize-16+i] ^= N[i]; // Last block in buffer XOR nonce N

	AES_init_ctx(&aesctx, ctx->k2); // initialize AES context with subkey k2
	AES_ECB_encrypt(&aesctx, &buf[bufsize-16]); //encrypt last buffer block with k2
	memcpy(T, &buf[bufsize-16], 16); //copy last buffer block to T



	// Encrypt Plaintext P:
	// ------------------------------------------------

	AES_init_ctx_iv(&aesctx, ctx->k3, T); // initialize AES context with subkey k3 and T as SIV
	//memset(buf, 0x00, p_rounded); // clear buffer
	memcpy(buf, P, p); // copy plaintext P into buffer
	AES_CTR_xcrypt_buffer(&aesctx, buf, p); // CTR-encrypt buffer
	memcpy(P, buf, p); // copy back encrypted buffer


	free(buf);
	return;
};





bool AES_CCM_SIV_decrypt(struct AES_CCM_SIV_ctx* ctx, uint8_t* A, uint32_t a, uint8_t* C, uint32_t p, uint8_t* N, uint8_t* T) {

	struct AES_ctx aesctx; //AES context for AES library
	uint8_t* buf; //buffer to hold pad(A), pad(P), L
	uint8_t* buf2; //buffer for temp storage of decrypted ciphertext
	uint32_t bufsize; //size of buffer  (unit: bytes)
	uint32_t a_rounded; //a rounded up to multiple of AES block size (unit: bytes)
	uint32_t p_rounded; //p rounded up to multiple of AES block size (unit: bytes)
	uint64_t L[2] = {0}; //Length block L that contains original bitlength of A and P
	uint8_t zeros[16] = {0}; //General purpose zero block
	volatile uint8_t error = 0; //Used for verifying the tag T

	// Decrypt ciphertext C:
	// ---------------------

	a_rounded = ((a+15)&~0x0F); //round up to multiple of AES block size
	p_rounded = ((p+15)&~0x0F); //round up to multiple of AES block size
	bufsize = a_rounded + p_rounded + 16; // len(pad(A)) + len(pad(P)) + len(L)
	buf = malloc(bufsize); //allocate memory for pad(A), pad(P), L
	buf2 = malloc(p); //allocate memory for temp storage of decrypted ciphertext
	memset(buf, 0x00, bufsize); //initialize buffer with zeros
	memcpy(&buf[a_rounded], C, p); //copy ciphertext into buffer
	AES_init_ctx_iv(&aesctx, ctx->k3, T); // initialize AES context with subkey k3 and T as SIV
	AES_CTR_xcrypt_buffer(&aesctx, &buf[a_rounded], p); // CTR-decrypt ciphertext in buffer
	memcpy(buf2, &buf[a_rounded], p); //copy decrypted ciphertext into temp buffer



	// Bijective Encoding Function (building up input for CBC-based PRF:
	// -----------------------------------------------------------------

	L[0] = (uint64_t)(a * 8); //original bitlength of A
	L[1] = (uint64_t)(p * 8); //original bitlength of P
	memcpy(buf, A, a); //copy additional data into buffer
	memcpy(&buf[a_rounded + p_rounded], &L[0], 8); //copy bitlength of A into L-block
	memcpy(&buf[a_rounded + p_rounded + 8], &L[1], 8); //copy bitlength of P into L-block



	// Calculate CBC-based PRF (MAC):
	// ------------------------------

	AES_init_ctx_iv(&aesctx, ctx->k1, zeros); // initialize AES context with subkey k1 and zero IV
        AES_CBC_encrypt_buffer(&aesctx, buf, bufsize); //encrypt buffer using CBC

        for(int i=0; i<16; i++)
                buf[bufsize-16+i] ^= N[i]; // Last block in buffer XOR nonce N

        AES_init_ctx(&aesctx, ctx->k2); // initialize AES context with subkey k2
        AES_ECB_encrypt(&aesctx, &buf[bufsize-16]); //encrypt last buffer block with k2




	// Verify calculated tag in buffer against received tag T:
	// -------------------------------------------------------

	for(int i=0; i<16; i++)
		error |= buf[bufsize-16+i] ^ T[i];

	if(error == 0) { // tags do match
		memcpy(C, buf2, p);
		free(buf);
		return 0;
	} else { // tags do not match
		free(buf);
		return 1;
	};
}
