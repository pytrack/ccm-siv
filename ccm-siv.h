
#ifndef _CCM_SIV_H_
#define _CCM_SIV_H_

//---------Defines-----------:

#define AES_CCM_SIV_KEYSIZE 128 // Here you can choose between 128, 192 and 256 bit keysize



//---------Includes----------:
#if AES_CCM_SIV_KEYSIZE == 128 // Defining appropriate key sizes for aes.h
	#define AES128 1
#elif AES_CCM_SIV_KEYSIZE == 192
	#define AES192 1
#else // 256
	#define AES256 1
#endif

#define ECB 1 // Defining appropriate modes of operations for aes.h
#define CBC 1
#define CTR 1

#include "aes.h"
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>


//---Variables/Structs------:
struct AES_CCM_SIV_ctx {
	uint8_t k1[AES_CCM_SIV_KEYSIZE/8]; // Subkey 1 (for CBC-based PRF)
	uint8_t k2[AES_CCM_SIV_KEYSIZE/8]; // Subkey 2 (for CBC-based PRF)
	uint8_t k3[AES_CCM_SIV_KEYSIZE/8]; // Subkey 3 (for CTR mode)
};

//---------Functions----------:
void AES_CCM_SIV_init(struct AES_CCM_SIV_ctx* ctx, uint8_t* key); //Subkey derivation
void AES_CCM_SIV_encrypt(struct AES_CCM_SIV_ctx* ctx, uint8_t* A, uint32_t a, uint8_t* P, uint32_t p, uint8_t* N, uint8_t* T); //Authenticated encryption
bool AES_CCM_SIV_decrypt(struct AES_CCM_SIV_ctx* ctx, uint8_t* A, uint32_t a, uint8_t* C, uint32_t p, uint8_t* N, uint8_t* T); //Authenticated decryption


#endif
